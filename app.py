from flask import Flask, send_from_directory
from flask_caching import Cache
from flask_compress import Compress
from blueprints.api import usersApi

# Initialization
app = Flask(__name__)
app.config.from_pyfile('config.cfg')

app.register_blueprint(usersApi, url_prefix="/api/v1/user")

Compress(app)
cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)

# Routes
@app.route('/<path:filename>')
def send_file(filename):
    return send_from_directory(app.static_folder, filename)

@app.route("/")
def index():
    return send_from_directory(app.static_folder, "index.min.html")

@app.route("/cv")
def cv():
    return send_from_directory(app.static_folder, "res/cv.pdf")

# 404 Error Handler
@app.errorhandler(404)
def not_found(error):
    return send_from_directory(app.static_folder, "404.min.html")

# Main
if __name__ == "__main__":
    app.run(app.config["IP"], app.config["PORT"])
