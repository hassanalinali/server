#!/usr/bin/env python
from migrate.versioning.shell import main

if __name__ == '__main__':
    main(repository='database_repository', url='sqlite:///database/main.db', debug='False')
