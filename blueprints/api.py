from flask import Blueprint, request, url_for
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker
from validate_email import validate_email
from utils.errors import errorById
from datetime import datetime
import utils.utils as utils
import json

usersApi = Blueprint('user', __name__)

Base = declarative_base()

from models.user import User

engine = create_engine('sqlite:///database/main.db')
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)

@usersApi.errorhandler(Exception)
def base_error(error):
    print(error)
    return json.dumps(errorById(1))


@usersApi.route('/create', methods=['POST'])
def user_create():
    session = DBSession()

    userEmail = request.form.get('email')
    userName = request.form.get('name')
    userPass = request.form.get('password')
    currentDate = datetime.now()
    
    emailExists = session.query(exists().where(User.email==userEmail)).scalar()

    if(userEmail is None or userName is None or userPass is None):
        return json.dumps(errorById(2))

    if(not validate_email(userEmail)): #verify=True for DNS verification, too slow though
        return json.dumps(errorById(3))

    if(emailExists):
        return json.dumps(errorById(5))

    user = User(email=userEmail, name=userName, password=userPass, registered_on=currentDate, confirmed=False)
    session.add(user)
    session.commit()

    token = utils.generate_confirmation_token(user.email)
    confirm_url = url_for('user.confirm_email', token=token, _external=True)
    utils.sendConfirmationEmail(userEmail, confirm_url)

    return json.dumps({'success': True})


@usersApi.route('/get', methods=['POST'])
def user_get_by_id():
    session = DBSession()

    userId = request.form.get('id')
    userEmail = request.form.get('email')

    if(userId is not None):
        currentUser = session.query(User).filter(User.id == userId).first()
        if(currentUser is None):
            return json.dumps(errorById(4))
        else:
            return json.dumps({'success': True, 'email': currentUser.email})

    elif(userEmail is not None):
        currentUser = session.query(User).filter(User.email == userEmail).first()
        if(currentUser is None):
            return json.dumps(errorById(4))
        else:
            return json.dumps({'success': True, 'email': currentUser.email})
        
    else:
        return json.dumps(errorById(4))


@usersApi.route('/delete', methods=['POST'])
def user_delete():
    session = DBSession()

    userId = request.form.get('id')
    userEmail = request.form.get('email')
    currentUser = None

    if(userEmail is None and userId is None):
        return json.dumps(errorById(2))

    if(userId is not None):
        currentUser = session.query(User).filter(User.id == userId).first()
        if(currentUser is None):
            return json.dumps(errorById(4))
    else:
        currentUser = session.query(User).filter(User.email == userEmail).first()
        if(currentUser is None):
            return json.dumps(errorById(4))

    return json.dumps({'success': True, 'email': currentUser.email})


@usersApi.route('/reset_password', methods=['POST'])
def user_reset_password():
    session = DBSession()
    
    userId = request.form.get('id')
    userEmail = request.form.get('email')
    newPass = utils.randomPass()
    currentUser = None

    if(userEmail is None and userId is None):
        return json.dumps(errorById(2))

    if(userId is not None):
        currentUser = session.query(User).filter(User.id == userId).first()
        if(currentUser is None):
            return json.dumps(errorById(4))
    else:
        currentUser = session.query(User).filter(User.email == userEmail).first()
        if(currentUser is None):
            return json.dumps(errorById(4))

    currentUser.password = newPass
    session.commit()
    utils.sendForgotPassEmail(currentUser.email, newPass)

    return json.dumps({'success': True})


@usersApi.route('/change_password', methods=['POST'])
def user_change_password():
    session = DBSession()
    
    userId = request.form.get('id')
    userEmail = request.form.get('email')
    oldPass = request.form.get('old_password')
    newPass = request.form.get('new_password')
    currentUser = None

    if(userEmail is None and userId is None):
        return json.dumps(errorById(2))

    if(userId is not None):
        currentUser = session.query(User).filter(User.id == userId).first()
        if(currentUser is None):
            return json.dumps(errorById(4))
    else:
        currentUser = session.query(User).filter(User.email == userEmail).first()
        if(currentUser is None):
            return json.dumps(errorById(4))

    if(currentUser.password == oldPass):
        currentUser.password = newPass
        session.commit()
        return json.dumps({'success': True})
    else:
        return errorById(6)


@usersApi.route('/confirm/<token>')
def confirm_email(token):
    #TODO: The user will see this, return user friendly interface html

    session = DBSession()

    try:
        email = utils.confirm_token(token)
    except:
        json.dumps({'success': False, 'error': 'Confirmation link invalid'})

    currentUser = session.query(User).filter(User.email == email).first()
    if(currentUser is None):
        return json.dumps(errorById(4))

    if currentUser.confirmed:
        return json.dumps({'success': False, 'error': 'User already confirmed'})
    else:
        currentUser.confirmed = True
        currentUser.confirmed_on = datetime.now()
        session.commit()
    return json.dumps({'success': True})