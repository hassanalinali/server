from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime
from blueprints.api import Base

class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String(250), nullable=False)
    name = Column(String(250), nullable=False)
    password = Column(String(250), nullable=False)
    registered_on = Column(DateTime(), nullable=False)
    confirmed = Column(Boolean(), nullable=False)
    confirmed_on = Column(DateTime(), nullable=True)