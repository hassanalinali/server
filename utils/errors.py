def errorById(x):
    id = str(x)

    return {
        '1': {'success': False, 'error_code': x, 'error_message': 'Unexpected error'},
        '2': {'success': False, 'error_code': x, 'error_message': 'Invalid parameters'},
        '3': {'success': False, 'error_code': x, 'error_message': 'Invalid user email'},
        '4': {'success': False, 'error_code': x, 'error_message': 'No record found'},    
        '5': {'success': False, 'error_code': x, 'error_message': 'Creditentials already in use'},
        '6': {'success': False, 'error_code': x, 'error_message': 'Invalid password'}
    }.get(id, 1)
    
    