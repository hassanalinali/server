from email.mime.text import MIMEText
from itsdangerous import URLSafeSerializer
from flask import current_app as app
import smtplib
import email.utils
import json
import random

def randomPass():

    s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
    passlen = 8
    p =  "".join(random.sample(s,passlen ))
    return p


def sendForgotPassEmail(email, newPass):

    to = email
    gmail_user = 'hassanalinali@gmail.com'
    gmail_pwd = 'sxixaewbikunsldp'
    smtpserver = smtplib.SMTP("smtp.gmail.com",587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(gmail_user, gmail_pwd)
    header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:Password reset \n'
    msg = header + '\n Your new password is ' + newPass + ' \n\n'

    try:
        smtpserver.sendmail(gmail_user, to, msg)
    finally:
        smtpserver.close()

def sendConfirmationEmail(email, url):
    to = email
    gmail_user = 'hassanalinali@gmail.com'
    gmail_pwd = 'sxixaewbikunsldp'
    smtpserver = smtplib.SMTP("smtp.gmail.com",587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(gmail_user, gmail_pwd)

    header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:Confirm Email \n'
    msg = header + '\n Please click this link to verify your email' + url + '\n\n'

    try:
        smtpserver.sendmail(gmail_user, to, msg)
    finally:
        smtpserver.close()


def generate_confirmation_token(email):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=app.config['SECURITY_PASSWORD_SALT']
        )
    except:
        return False
    return email